import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable} from'rxjs'; //טענו את אורזרבבול מתוך rxjs
import { AngularFireDatabase } from '@angular/fire/database'; //מוסיפים לדטהבייס
import {AngularFireList} from '@angular/fire/database';
import { map } from 'rxjs/operators';//לבדוק אם מחובר
import { TOUCH_BUFFER_MS } from '@angular/cdk/a11y';



// כל הפונקציות של הרשמה והתחברות נמצאות בהוס
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private fireBaseAuth:AngularFireAuth,
  private db:AngularFireDatabase) {
    //נכניס תוכן שבעצם יכניס תוכן לתוך המשתנה יוזר מתוך הפיירבייס
    this.user=fireBaseAuth.authState; //דיס יוזר לא באמת שייך למחלקה כי הוא מסוג אורזרבבל-הוס סטייט, מסדר את הבעיה עם דיס. יוזר
//מאזין להוס סטייט
   }
  //------------------------------------------------------------------------------------
  signup(email:string,password:string){
  return this.fireBaseAuth.auth.createUserWithEmailAndPassword(email,password)

  }
  //------------------------------------------------------------------------------------
  updateProfile(user,name:string){ //פונקציה ליצירת שם
    user.updateProfile({displayName:name,photoURL:''})
  }
  //------------------------------------------------------------------------------------
  login(email:string,password:string){ //לצורך ביצוע לוגאין
    return this.fireBaseAuth.auth.signInWithEmailAndPassword(email,password)
  }
  //------------------------------------------------------------------------------------
  logout(){
    return this.fireBaseAuth.auth.signOut();
  }
  //------------------------------------------------------------------------------------
  isAuth()
  {
  return this.fireBaseAuth.auth.currentUser; //מחזיר נאל אם אין יוזר
  }
  //------------------------------------------------------------------------------------
  user:Observable<firebase.User>; //יוזר הוא אותו היוזר של פיירבייס

  
  addUser(user,name:string){ //נעשה כדי שכל יוזר ילך לטודוס שלו
    let uid=user.uid;
    let  ref=this.db.database.ref('/');  
    //יצירת אנדפונט ליוזר הספציפי
    ref.child('user').child(uid).push({'name':name}); //ברגע שהרצנו את הפקודה הזו למעשה יצרנו אנד פויירט חדש (הוא בהכרח חדש כי זה חלק של ההרשמה)
  }
}
