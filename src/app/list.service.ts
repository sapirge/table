import { Injectable } from '@angular/core';
import {AuthService} from './auth.service';//נוסיף
import { AngularFireDatabase } from '@angular/fire/database';//נוסיף
import { all } from 'q';

@Injectable({
  providedIn: 'root'
})
export class ListService {


  constructor(private authService:AuthService,
    private db:AngularFireDatabase,) { } //נוסיף
  //------------------------------------------------------------------------------------
  addProduct(name,color,price,currency){ //מקבלים טקסט מהTS
  
    this.authService.user.subscribe(user=>{ //מגיעים ליוזר
      this.db.list('/user/'+user.uid+'/products/').push({'name':name, 'color':color,'price':price,'currency':currency});//דוחפים לפיירבייס
    })
  }
    //------------------------------------------------------------------------------------



  delete(key:string)
  {
    this.authService.user.subscribe(user => {
      this.db.list('/user/'+user.uid+'/products/').remove(key);
    })
  }


  //------------------------------------------------------------------------------------

  updateName(key,name){
    this.authService.user.subscribe(user=>
    this.db.list('/user/'+user.uid+'/products').update(key,{'name':name})
    )  
  }
  updateColor(key,color){
    this.authService.user.subscribe(user=>
    this.db.list('/user/'+user.uid+'/products').update(key,{'color':color})
    )  
  }
  updatePrice(key,price){
    this.authService.user.subscribe(user=>
    this.db.list('/user/'+user.uid+'/products').update(key,{'price':price})
    )  
  }
  updateCurrency(key,currency){
    this.authService.user.subscribe(user=>
    this.db.list('/user/'+user.uid+'/products').update(key,{'currency':currency})
    )  
  }
    //------------------------------------------------------------------------------------

}





